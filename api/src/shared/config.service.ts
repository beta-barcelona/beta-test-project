import { Injectable } from '@nestjs/common';
import convict from 'convict';

const schema = {
  env: {
    doc: 'The application environment.',
    format: ['production', 'development', 'staging', 'test'],
    default: 'development',
    env: 'NODE_ENV',
  },
  host: {
    doc: 'The IP address to bind.',
    format: 'ipaddress',
    default: '0.0.0.0',
    env: 'IP_ADDRESS',
  },
  port: {
    doc: 'The port to bind.',
    format: 'port',
    default: 3000,
    env: 'PORT',
  },
  logging: {
    level: {
      doc: 'Log level',
      env: 'LOG_LEVEL',
      default: 'info',
    },
  },
  db: {
    url: {
      doc: 'Postgres url',
      env: 'POSTGRES_URL',
      default: 'postgres://foo:bar@db/baz',
      sensitive: true
    },
    ssl: {
      enabled: {
        doc: 'Enables SSL connections',
        env: 'POSTGRES_SSL',
        default: false
      },
      ca: {
        doc: 'Certificate Authority',
        env: 'POSTGRES_CA',
        default: '',
        sensitive: true
      }
    }
  },
  jwt: {
    secret: {
      doc: 'JWT secret',
      env: 'JWT_SECRET',
      default: 'changeme',
      sensitive: true,
    },
    expiresIn: {
      doc: 'JWT expiry in seconds',
      env: 'JWT_EXPIRES_IN',
      format: 'int',
      default: 7200,
    },
  },
  graphql: {
    playground: {
      doc: 'Whether to enable to disable the graphql playground',
      env: 'GRAPHQL_PLAYGROUND',
      default: false,
    },
  },
  cloudinary: {
    cloud: {
      doc: 'Cloud Name',
      env: 'CL_CLOUD_NAME',
      default: 'changeme',
    },
    key: {
      doc: 'Api Key',
      env: 'CL_API_KEY',
      default: 'changeme',
      sensitive: true,
    },
    secret: {
      doc: 'Api Secret',
      env: 'CL_API_SECRET',
      default: 'changeme',
      sensitive: true,
    },
  },
  baseUrls: {
    admin: {
      doc: 'Admin base url',
      env: 'BASE_URL_ADMIN',
      default: 'http://admin.berta-models.docker',
    },
    frontend: {
      doc: 'Frontend base url',
      env: 'BASE_URL_FRONTEND',
      default: 'http://berta-models.docker',
    },
    api: {
      doc: 'API base url',
      env: 'BASE_URL_API',
      default: 'http://api.berta-models.docker'
    },
    pdfAssets: {
      doc: 'Pdf assets base url',
      env: 'BASE_URL_PDF',
      default: 'http://frontend:4200'
    }
  },
  replyTo: {
    doc: 'Reply to for system emails',
    env: 'REPLY_TO',
    default: 'No Reply <noreply@beta.barcelona>',
  },
  smtp: {
    host: {
      doc: 'Hostname',
      env: 'SMTP_HOST',
      default: 'mailhog',
    },
    secure: {
      doc: 'Whether or not to use a secure connection',
      env: 'SMTP_SECURE',
      format: 'Boolean',
      default: false,
    },
    port: {
      doc: 'Port to use',
      env: 'SMTP_PORT',
      format: 'port',
      default: 1025,
    },
    auth: {
      user: {
        doc: 'Username',
        env: 'SMTP_USERNAME',
        default: 'noreply@localhost',
        sensitive: true,
      },
      pass: {
        doc: 'Username',
        env: 'SMTP_PASSWORD',
        default: 'password',
        sensitive: true,
      },
    },
  },
};

export interface GraphqlConfig {
  playground: boolean;
}

export interface JwtConfig {
  secret: string;
  expiresIn: number;
}

export interface BaseUrls {
  admin: string;
  frontend: string;
  api: string;
  pdfAssets: string
}

export interface DbConfig {
  url: string;
  ssl: {
    enabled: boolean
    ca: String
  }
}

@Injectable()
export class ConfigService {

  public config: convict.Config<any>;

  constructor() {
    const config = this.config = convict(schema);
    config.loadFile(`./config/${this.env}.json`);
    config.validate({ allowed: 'strict' });

    const { env } = this;

    if (env === 'development') {
      console.log('Configuration');
      console.log(config.toString());
    }

  }

  get env(): string {
    return this.config.get<string>('env');
  }

  get host(): string {
    return this.config.get('host');
  }

  get port(): number {
    return +this.config.get('port');
  }

  get graphql(): GraphqlConfig {
    return this.config.get('graphql');
  }

  get jwt(): JwtConfig {
    return this.config.get('jwt');
  }

  get baseUrls(): BaseUrls {
    return this.config.get('baseUrls');
  }

  get db(): DbConfig {
    return this.config.get('db');
  }

}
