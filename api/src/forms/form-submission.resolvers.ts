import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UseGuards, ValidationPipe } from '@nestjs/common';
import { JwtAuthGuard } from '@app/auth/guards/jwt-auth.guard';
import { FormSubmissionService } from '@app/forms/form-submission.service';
import { FormSubmissionDto } from '@app/forms/dto/form-submission.dto';
import { UpsertFormSubmissionDto } from '@app/forms/dto/upsert-form-submission.dto';
import { FormSubmissionEntity } from '@app/orm/entities/form-submission.entity';
import {FormConfigService} from "@app/forms/form-config.service";

@Resolver('FormSubmission')
export class FormSubmissionResolvers {
	constructor(private readonly formSubmissionService: FormSubmissionService,
                private readonly formConfigService: FormConfigService) {}

	@Query()
	@UseGuards(JwtAuthGuard)
	async formSubmissions(@Args('offset') offset: number, @Args('limit') limit: number, @Args('q') q: string) {
		const entities = await this.formSubmissionService.find({ offset, limit, q });
		return entities.map(e => new FormSubmissionDto(e));
	}

	@Query()
	@UseGuards(JwtAuthGuard)
	async formSubmissionsCount(@Args('offset') offset: number, @Args('limit') limit: number, @Args('q') q: string) {
		return await this.formSubmissionService.count({ offset, limit, q });
	}

	@Query()
	@UseGuards(JwtAuthGuard)
	async formSubmission(@Args('id') id: number) {
		const entity = await this.formSubmissionService.findById(id);
		return entity ? new FormSubmissionDto(entity) : null;
	}

	@Mutation()
	async createFormSubmission(@Args('formSubmission') dto: UpsertFormSubmissionDto) {
		const entity = new FormSubmissionEntity(dto);
		const created = await this.formSubmissionService.create(entity);

		const formConfig = await this.formConfigService.findById(created.formConfig.id);

		await this.formSubmissionService.sendFormDataEmails(created, formConfig);

		if (formConfig.name === 'contact') {
		    await this.formSubmissionService.createModelContent(created);
        }

		return new FormSubmissionDto(created);
	}

	@Mutation()
	@UseGuards(JwtAuthGuard)
	async updateFormSubmissionById(@Args('id') id: number, @Args('formSubmission') dto: UpsertFormSubmissionDto) {
		const entity = new FormSubmissionEntity(dto);
		new FormSubmissionDto(await this.formSubmissionService.updateById(id, entity));
	}

	@Mutation()
	@UseGuards(JwtAuthGuard)
	async deleteFormSubmissionById(@Args('id') id: number) {
		return await this.formSubmissionService.deleteById(id);
	}
}
