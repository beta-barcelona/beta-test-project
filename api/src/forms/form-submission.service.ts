import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {FormSubmissionEntity} from "@app/orm/entities/form-submission.entity";
import {QueryFilter} from "@app/shared/query-filter";
import {FormConfigEntity} from "@app/orm/entities/form-config.entity";
import {EmailService} from "@app/shared/email.service";
import {ConfigService} from "@app/shared/config.service";
import * as Mustache from 'mustache';
import {ContentService} from "@app/content/content.service";
import {ContentEntity} from "@app/orm/entities/content.entity";

@Injectable()
export class FormSubmissionService {

    constructor(
        @InjectRepository(FormSubmissionEntity)
        private readonly formSubmissionRepository: Repository<FormSubmissionEntity>,
        @InjectRepository(FormConfigEntity)
        private readonly formConfigRepository: Repository<FormConfigEntity>,
        private readonly emailService: EmailService,
        private readonly configService: ConfigService,
        private readonly contentService: ContentService
    ){}

    async find(filter: QueryFilter): Promise<FormSubmissionEntity[]> {

        const { offset, limit, q, formConfig} = filter;

        const builder = this.formSubmissionRepository
            .createQueryBuilder('formSubmission')
            .leftJoinAndSelect('formSubmission.formConfig', 'formConfig');
        // TODO subselect only name and id fields

        if (q) {
            builder
                .where('formConfig.name ~* :query')
                .setParameter('query', `.*${q}.*`);
        }

        if(formConfig) {
            builder
                .where('formConfig.name = :formConfig')
                .setParameter('formConfig', formConfig)
        }

        return await builder
            .offset(offset)
            .limit(limit)
            .orderBy('formSubmission.id', 'ASC')
            .getMany();
    }

    async count(filter: QueryFilter): Promise<number> {

        const {q, formConfig} = filter;

        const builder = this.formSubmissionRepository
            .createQueryBuilder('formSubmission')
            .leftJoinAndSelect('formSubmission.formConfig', 'formConfig');
        // TODO subselect only name and id fields

        if (q) {
            builder
                .where('formConfig.name ~* :query')
                .setParameter('query', `.*${q}.*`);
        }

        if(formConfig) {
            builder
                .where('formConfig.name = :formConfig')
                .setParameter('formConfig', formConfig)
        }

        return await builder.getCount();
    }

    async create(formSubmission: FormSubmissionEntity): Promise<FormSubmissionEntity> {
        return this.formSubmissionRepository.save(formSubmission);
    }

    async findById(id: number): Promise<FormSubmissionEntity | undefined> {
        return await this.formSubmissionRepository.findOne(id);
    }

    async updateById(id: number, formSubmission: FormSubmissionEntity): Promise<FormSubmissionEntity | undefined> {

        const count = await this.formSubmissionRepository.count({ id });
        if (count === 0) return undefined;

        await this.formSubmissionRepository.update(id, formSubmission);

        return await this.findById(id);
    }

    async deleteById(id: number): Promise<boolean> {

        const count = await this.formSubmissionRepository.count({ id });

        if (count === 0) return false;

        await this.formSubmissionRepository.delete(id, { });

        return true;
    }

    async sendFormDataEmails(formSubmission: FormSubmissionEntity,
                             formConfig: FormConfigEntity): Promise<boolean> {

        if (!formConfig.emailConfig) { return false; }

        const {emailRecipients, subject, template} = formConfig.emailConfig;

        const {emails, fields} = emailRecipients;

        const to = emails || [];

        // Add recipients from form data

        if (fields && fields.length) {
            for (let field of fields) {
                to.push(formSubmission.data[field]);
            }
        }

        const frontendBaseUrl = this.configService.config.get('baseUrls.frontend');
        const adminBaseUrl = this.configService.config.get('baseUrls.admin');

        const data = {...formSubmission.data, frontendBaseUrl, adminBaseUrl};

        const html = Mustache.render(template, data);

        const message = {
            to,
            subject: subject || 'Form Submitted',
            html
        };

        await this.emailService.send(message);

        return true;

    }

    async createModelContent(formSubmission: FormSubmissionEntity) {

        const formData = formSubmission.data;
        const { name, surname, email, gender, height } = formData;

        const title = `${name} ${surname}`

        const contentData: any = {
            title,
            meta: [],
            slug: this.slugify(title, '-'),
            contentTypeId: 1,
            data: {
                name: `${name} ${surname}`,
                email, gender, height,
                images: []
            }
        };

        const shots = ['shot1', 'shot2', 'shot3', 'shot4', 'shot5'];
        for (let shot of shots) {
            const file = formData[shot];
            if (file && file.id) {
                contentData.data.images.push({id: file.id});
            }
        }

        await this.contentService.create(new ContentEntity(contentData));

    }

    slugify(text, separator) {
        text = text.toString().toLowerCase().trim();

        const sets = [
            {to: 'a', from: '[ÀÁÂÃÄÅÆĀĂĄẠẢẤẦẨẪẬẮẰẲẴẶ]'},
            {to: 'c', from: '[ÇĆĈČ]'},
            {to: 'd', from: '[ÐĎĐÞ]'},
            {to: 'e', from: '[ÈÉÊËĒĔĖĘĚẸẺẼẾỀỂỄỆ]'},
            {to: 'g', from: '[ĜĞĢǴ]'},
            {to: 'h', from: '[ĤḦ]'},
            {to: 'i', from: '[ÌÍÎÏĨĪĮİỈỊ]'},
            {to: 'j', from: '[Ĵ]'},
            {to: 'ij', from: '[Ĳ]'},
            {to: 'k', from: '[Ķ]'},
            {to: 'l', from: '[ĹĻĽŁ]'},
            {to: 'm', from: '[Ḿ]'},
            {to: 'n', from: '[ÑŃŅŇ]'},
            {to: 'o', from: '[ÒÓÔÕÖØŌŎŐỌỎỐỒỔỖỘỚỜỞỠỢǪǬƠ]'},
            {to: 'oe', from: '[Œ]'},
            {to: 'p', from: '[ṕ]'},
            {to: 'r', from: '[ŔŖŘ]'},
            {to: 's', from: '[ßŚŜŞŠ]'},
            {to: 't', from: '[ŢŤ]'},
            {to: 'u', from: '[ÙÚÛÜŨŪŬŮŰŲỤỦỨỪỬỮỰƯ]'},
            {to: 'w', from: '[ẂŴẀẄ]'},
            {to: 'x', from: '[ẍ]'},
            {to: 'y', from: '[ÝŶŸỲỴỶỸ]'},
            {to: 'z', from: '[ŹŻŽ]'},
            {to: '-', from: '[·/_,:;\']'}
        ];

        sets.forEach(set => {
            text = text.replace(new RegExp(set.from,'gi'), set.to);
        });

        text = text.toString().toLowerCase()
            .replace(/\s+/g, '-')         // Replace spaces with -
            .replace(/&/g, '-and-')       // Replace & with 'and'
            .replace(/[^\w\-]+/g, '')     // Remove all non-word chars
            .replace(/\--+/g, '-')        // Replace multiple - with single -
            .replace(/^-+/, '')           // Trim - from start of text
            .replace(/-+$/, '');          // Trim - from end of text

        if ((typeof separator !== 'undefined') && (separator !== '-')) {
            text = text.replace(/-/g, separator);
        }

        return text;
    }


}
