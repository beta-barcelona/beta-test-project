import {assignClean} from '@app/shared/utils';
import {FormConfig} from "@app/graphql/schema";

export class FormConfigDto extends FormConfig {

    constructor(data: any){
        super();
        assignClean(this, data);
    }

}
