import {Setting} from '@app/graphql/schema';
import {assignClean} from '@app/shared/utils';

export class SettingDto extends Setting {

    constructor(data: any) {
        super();
        assignClean(this, data);
    }

}
