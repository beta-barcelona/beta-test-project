import {Args, Mutation, Query, Resolver} from '@nestjs/graphql';
import {UseGuards, UsePipes, ValidationPipe} from '@nestjs/common';
import {SettingService} from "@app/setting/setting.service";
import {UpdateSettingDto} from '@app/setting/dto/update-setting.dto';
import {SettingDto} from '@app/setting/dto/setting.dto';
import {SettingEntity} from '@app/orm/entities/setting.entity';
import {JwtAuthGuard} from '@app/auth/guards/jwt-auth.guard';

@Resolver('Setting')
export class SettingResolvers {

    constructor(private readonly settingService: SettingService) {}

    @Query()
    async settings(@Args('offset') offset: number,
                   @Args('limit') limit: number,
                   @Args('q') q: string) {
        const entities = await this.settingService.find({ offset, limit, q });
        return entities.map(e => new SettingDto(e));
    }

    @Query()
    async settingsCount(@Args('offset') offset: number,
                        @Args('limit') limit: number,
                        @Args('q') q: string) {
        return await this.settingService.count({ offset, limit, q });
    }

    @Query('setting')
    async setting(@Args('id') id: string) {
        const entity = await this.settingService.findById(id);
        return entity ? new SettingDto(entity) : null;
    }

    @Mutation()
    @UseGuards(JwtAuthGuard)
    @UsePipes(ValidationPipe)
    async updateSettings(@Args('settings') dtos: [UpdateSettingDto]) {
        const entities = dtos.map(d => new SettingEntity(d));
        const updated = await this.settingService.updateAll(entities);
        return updated.map(u => new SettingDto(u));
    }

}
