import {Injectable} from '@nestjs/common';

import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';

import * as jwt from 'jsonwebtoken';
import {UserEntity} from '../orm/entities/user.entity';
import {EmailService} from "../shared/email.service";
import {JwtTokenDto} from '@app/auth/dto/jwt-token.dto';
import {AuthRequest} from '@app/auth/auth-request';
import {AccessTokenDto} from '@app/auth/dto/access-token.dto';
import {ConfigService} from '@app/shared/config.service';

export class UserNotFound extends Error {

    constructor(message?: string){
        super(message);
        Object.setPrototypeOf(this, new.target.prototype);
    }

}

export class PasswordIncorrect extends Error {
    constructor(message?: string){
        super(message);
        Object.setPrototypeOf(this, new.target.prototype);
    }
}

@Injectable()
export class AuthService {

    private readonly jwtExpiresIn: number;
    private readonly jwtSecret: string;

    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
        private readonly emailService: EmailService,
        private readonly configService: ConfigService
    ) {
        this.jwtExpiresIn = configService.config.get('jwt.expiresIn');
        this.jwtSecret = configService.config.get('jwt.secret');
    }

    async authenticate(request: AuthRequest): Promise<AccessTokenDto | UserNotFound | PasswordIncorrect> {

        const {email, password, type} = request;

        switch (type) {

            case 'userpass':

                const user = await this.userRepository
                    .createQueryBuilder('u')
                    .select(['u.id', 'u.password'])
                    .where('u.email = :email')
                    .setParameters({email})
                    .getOne();

                if (!user) return new UserNotFound();

                const checkPassword = await user.checkPassword(password!);
                if (!checkPassword) return new PasswordIncorrect();

                return this.createToken(user.id);

            default:
                throw new Error(`Unhandled request type: ${request.type}`);
        }
    }

    async createToken(id: number, expiresInSeconds?: number): Promise<AccessTokenDto> {

        const user = await this.userRepository.findOne(id);

        const {firstName, familyName} = user!;

        const   expiresIn = expiresInSeconds || this.jwtExpiresIn,
                secretOrKey = this.jwtSecret;

        const token = jwt.sign({sub: id, firstName, familyName}, secretOrKey, {expiresIn});

        return new AccessTokenDto({
            id: token,
            expiresIn,
            user,
        });
    }

    async validateUser(payload: JwtTokenDto): Promise<boolean> {

        const {sub} = payload;

        const count = await this.userRepository.count({id: sub});
        return count === 1;
    }

    async requestPasswordReset(email: string): Promise<boolean> {

        const expirySeconds = 3600 * 2; // 2 hours

        const user = await this.userRepository.findOne({ email });
        if(!user) return false;

        const token = await this.createToken(user.id, expirySeconds);

        const baseUrl = this.configService.config.get('baseUrls.admin');
        const url = `${baseUrl}/reset-password?access_token=${token.id}`;

        const message = {
            to: email,
            subject: 'Password reset request',
            html: `To reset your password click on the following link: \n\n<a href="${url}">Reset Password</a>`
        };

        return this.emailService.send(message);
    }

}
