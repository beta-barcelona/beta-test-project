import { assignClean } from '@app/shared/utils';
import {FormContent} from '@app/graphql/schema';


export class FormContentDto extends FormContent {

    constructor(data: any) {
        super();
        assignClean(this, data);
    }

}
