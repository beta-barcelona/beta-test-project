import {
    registerDecorator,
    ValidationArguments,
    ValidationOptions,
    ValidatorConstraint,
    ValidatorConstraintInterface
} from 'class-validator';
import {forwardRef, Inject, Injectable} from '@nestjs/common';
import {ContentService} from "./content.service";


@Injectable()
@ValidatorConstraint({ async: true })
export class SlugIsNotAlreadyTakenConstraint implements ValidatorConstraintInterface {

    constructor(
        @Inject(forwardRef(() => ContentService))
        private readonly contentService: ContentService){
    }

    async validate(slug: string, validationArguments?: ValidationArguments): Promise<boolean> {
        const content = await this.contentService.findBySlug(slug);
        console.log('Validating slug', slug, !!content);
        return !content;
    }

}

export function SlugIsNotAlreadyTaken(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: SlugIsNotAlreadyTakenConstraint
        });
    };
}
