import {Args, Mutation, Query, Resolver} from '@nestjs/graphql';
import {ContentTypeService} from '@app/content/content-type.service';
import {ContentTypeEntity} from '@app/orm/entities/content-type.entity';
import {UseGuards, ValidationPipe} from '@nestjs/common';
import {ContentTypeDto} from '@app/content/dto/content-type.dto';
import {UpsertContentTypeDto} from '@app/content/dto/upsert-content-type.dto';
import {JwtAuthGuard} from '@app/auth/guards/jwt-auth.guard';

@Resolver('ContentType')
@UseGuards(JwtAuthGuard)
export class ContentTypeResolvers {

    constructor(private readonly contentTypeService: ContentTypeService) {}

    @Query()
    async contentTypes(@Args('offset') offset: number,
                       @Args('limit') limit: number,
                       @Args('q') q: string) {
        const entities = await this.contentTypeService.find({ offset, limit, q});
        return entities.map(e => new ContentTypeDto(e));
    }

    @Query()
    async contentTypesCount(@Args('offset') offset: number,
                            @Args('limit') limit: number,
                            @Args('q') q: string) {
        return await this.contentTypeService.count({ offset, limit, q});
    }

    @Query()
    async contentType(@Args('id') id: number) {
        const entity = await this.contentTypeService.findById(id);
        return entity ? new ContentTypeDto(entity) : null;
    }

    @Mutation()
    async createContentType(@Args('contentType', new ValidationPipe()) dto: UpsertContentTypeDto ) {
        const entity = new ContentTypeEntity(dto);
        return new ContentTypeDto(await this.contentTypeService.create(entity));
    }

    @Mutation()
    async updateContentTypeById(@Args('id') id: number,
                                @Args('content') dto: UpsertContentTypeDto) {
        const entity = new ContentTypeEntity(dto);
        new ContentTypeDto(await this.contentTypeService.updateById(id, entity));
    }

    @Mutation()
    async deleteContentTypeById(@Args('id') id: number) {
        return await this.contentTypeService.deleteById(id);
    }

}

