import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {ContentTypeEntity} from './content-type.entity';
import {assignClean} from '../../shared/utils';

@Entity('content')
export class ContentEntity {

    constructor(data: any) {
        assignClean(this, data);

        if(data && data.contentTypeId) {
            this.contentType = new ContentTypeEntity({ id: data.contentTypeId });
        }
    }

    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ length: 64 })
    title: string;

    @Column({ type: 'jsonb' })
    meta: any;

    @Column({ length: 64 })
    slug: string;

    @Column({ type: 'jsonb' })
    data: any;

    @Column({ type: 'timestamp', default: () => null })
    publishedAt?: Date;


    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    createdAt: Date;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    updatedAt: Date;

    @ManyToOne(type => ContentTypeEntity, contentType => contentType.content, {eager: true})
    contentType: ContentTypeEntity;
}
