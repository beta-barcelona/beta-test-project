import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { assignClean } from '../../shared/utils';
import { FormConfigEntity } from '@app/orm/entities/form-config.entity';

@Entity('form_submission')
export class FormSubmissionEntity {
	constructor(data: any) {
		assignClean(this, data);

		if (data && data.formConfigId) {
			this.formConfig = new FormConfigEntity({ id: data.formConfigId });
		}
	}

	@PrimaryGeneratedColumn('increment')
	id: number;

	@Column({ type: 'jsonb' })
	data: any;

	@Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
	createdAt: Date;

	@Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
	updatedAt: Date;

	@ManyToOne(type => FormConfigEntity, formConfig => formConfig.formSubmissions, { eager: true })
	formConfig: FormConfigEntity;
}
