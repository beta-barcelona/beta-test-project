import {Column, Entity, PrimaryColumn} from 'typeorm';

import {assignClean} from '../../shared/utils';

@Entity('setting')
export class SettingEntity {

    constructor(data: any) {
        assignClean(this, data);
    }

    @PrimaryColumn()
    id: string;

    @Column({length: 64})
    value: string;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
    createdAt: Date;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
    updatedAt: Date;
}


