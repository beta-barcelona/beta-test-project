import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { FileEntity } from '../orm/entities/file.entity';


import { FileUploadDto } from './file-upload.dto';
import { ConfigService } from '@app/shared/config.service';

const cloudinary = require('cloudinary');

@Injectable()
export class FileService {

    constructor(@InjectRepository(FileEntity)
    private readonly fileRepository: Repository<FileEntity>,
        private readonly configService: ConfigService) {
    }

    async create(fileUpload: FileUploadDto, ownerId?: number): Promise<FileEntity> {

        const { mimetype } = fileUpload;
        const [resource_type, format] = mimetype.split('/');

        const folder = this.configService.env;

        const response = await new Promise<any>((resolve, reject) => {

            cloudinary.v2.uploader.upload_stream(
                { resource_type, format, folder, timeout: 300000 },
                (err: Error, result: any) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(result);
                }
            ).end(fileUpload.buffer);

        });

        const { public_id, width, height, bytes, version, signature, url, secure_url } = response;

        const metadata = { version, signature, width, height, bytes, url, secure_url };

        // create various utility urls

        if (resource_type === 'video') {

            Object.assign(metadata, {
                url_preview: secure_url.replace(format, 'png'),
                url_mp4: secure_url.replace(format, 'mp4'),
                url_webm: secure_url.replace(format, 'webm'),
                url_ogg: secure_url.replace(format, 'ogg')
            });

        }

        //

        const cloudName = this.configService.config.get('cloudinary.cloud');

        const modelData: any = {
            mimetype: mimetype,
            filename: fileUpload.originalname,
            clName: cloudName,
            clPublicId: public_id,
            metadata
        };

        if (ownerId) {
            modelData.owner = {
                id: ownerId
            };

        }

        const model = new FileEntity(modelData);

        return await this.fileRepository.save(model);
    }

    async findOne(id: number): Promise<FileEntity | undefined> {
        return this.fileRepository.findOne(id);
    }

    async findByIds(ids: number[]): Promise<FileEntity[]> {
        return this.fileRepository.findByIds(ids);
    }

    async update(file: FileEntity): Promise<FileEntity | undefined> {
        return this.fileRepository.save(file);
    }

    async updateById(id: number, file: FileEntity): Promise<FileEntity | undefined> {
        const count = await this.fileRepository.count({ id });
        if (count === 0) return undefined;

        file.updatedAt = new Date();

        await this.fileRepository.update(id, file);

        return await this.findOne(id);
    }

}
