import { Controller, FileInterceptor, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileService } from "@app/file/file.service";
import { FileUploadDto } from '@app/file/file-upload.dto';
import { JwtToken } from '@app/auth/decorators/jwt-token.decorator';
import { JwtTokenDto } from '@app/auth/dto/jwt-token.dto';

@Controller('file')
export class FileController {

    constructor(private fileService: FileService) { }

    @Post()
    @UseInterceptors(FileInterceptor('file'))
    async create(@UploadedFile() file: FileUploadDto, @JwtToken() user: JwtTokenDto) {
        const userId = user ? user.sub : undefined;
        return this.fileService.create(file, userId);
    }

}
