import { Module } from '@nestjs/common';
import { FileService } from './file.service';
import { FileController } from './file.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {AuthModule} from '../auth/auth.module';
import {FileEntity} from '../orm/entities/file.entity';
import {FileResolvers} from './file.resolvers';

@Module({
    imports: [
        AuthModule,
        TypeOrmModule.forFeature([FileEntity])
    ],
    controllers: [FileController],
    providers: [FileService, FileResolvers],
    exports: [FileService]
})
export class FileModule {

}
