import {NgModule} from '@angular/core';
import {UsersListComponent} from './users-list.component';
import {UsersRoutesModule} from './users.routes';
import {CommonModule} from '@angular/common';
import {LayoutModule} from '../layout/layout.module';
import {FormlyModule} from '@ngx-formly/core';
import {ReactiveFormsModule} from '@angular/forms';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {UserDetailComponent} from './user-detail.component';
import {TimeagoModule} from 'ngx-timeago';
import {IconsModule} from '../icons.module';
import {CrudModule} from '../crud/crud.module';


@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    ReactiveFormsModule,
    NgbPaginationModule,
    FormlyModule,
    UsersRoutesModule,
    TimeagoModule,
    IconsModule,
    CrudModule
  ],
  declarations: [
    UsersListComponent,
    UserDetailComponent
  ]
})
export class UsersModule {

}
