import {LoginComponent} from './login.component';
import {PasswordResetRequestComponent} from "./password-reset-request.component";
import {ResetPasswordComponent} from "./reset-password.component";

export const authRoutes = [

  { path: 'login', component: LoginComponent },
  { path: 'password-reset-request', component: PasswordResetRequestComponent },
  { path: 'reset-password', component: ResetPasswordComponent }

];
