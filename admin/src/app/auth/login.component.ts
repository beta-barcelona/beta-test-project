import {Component} from '@angular/core';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormGroup, Validators} from '@angular/forms';
import {BaseComponent} from '../shared/base.component';
import {Apollo} from 'apollo-angular';
import {Router} from '@angular/router';
import {flatMap} from 'rxjs/operators';
import {ApolloError} from "apollo-client";
import {authenticateWithEmail} from '@app/auth/auth.graphql';
import {AuthService} from '@app/auth/auth.service';


@Component({
  selector: 'app-login',
  styles: [`
    form {

      max-width: 330px;
      width: 100%;
      padding: 15px;
      margin: 200px auto auto auto;

    }
  `],
  template: `
    <div>
      
      <form [formGroup]="form" (ngSubmit)="onSubmit()">
        <ngb-alert *ngIf="errorText" type="danger" [dismissible]="false">
          {{ errorText }}
        </ngb-alert>
        <formly-form [form]="form" [fields]="fields" [model]="model">
          <button type="submit" class="btn btn-primary btn-block" [disabled]="form.invalid">Login</button>
        </formly-form>
        <a routerLink="/password-reset-request">Reset Password</a>
      </form>
      
    </div>
  `
})
export class LoginComponent extends BaseComponent {

  fields: FormlyFieldConfig[] = [{
    type: 'input',
    key: 'email',
    templateOptions: {
      placeholder: 'Email',
      required: true,
    },
    validators: {
      validation: [Validators.email],

    }
  },
    {
      type: 'input',
      key: 'password',
      templateOptions: {
        placeholder: 'Password',
        required: true,
        type: 'password'
      }
    }
  ];

  form = new FormGroup({});

  model: any = {};

  errorText?: string;

  constructor(private readonly apollo: Apollo,
              private readonly router: Router,
              private readonly authService: AuthService) {
    super();
  }

  onSubmit() {

    this.errorText = null;

    this.apollo.mutate({
      mutation: authenticateWithEmail,
      variables: this.model
    }).subscribe(
      ({ data }) => {

        const {id, expiresIn} = data.authenticateWithEmail;

        this.authService.setAccessToken(id, expiresIn);
        this.router.navigateByUrl('/dashboard');

      },
      (error) => {

        if (error instanceof ApolloError) {

          // Naive assumption for now that it's only auth error
          this.errorText = 'Email or password is incorrect';

        } else {

          this.errorText = 'Unexpected Server error. Please contact the support team';

        }

      });

  }

}
