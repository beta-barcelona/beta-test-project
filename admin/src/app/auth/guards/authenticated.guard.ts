import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';

import {Injectable} from '@angular/core';
import {NGXLogger} from 'ngx-logger';
import {Apollo} from 'apollo-angular';
import {AuthService} from '@app/auth/auth.service';

@Injectable()
export class AuthenticatedGuard implements CanActivate {

  constructor(private apollo: Apollo,
              private router: Router,
              private authService: AuthService,
              private log: NGXLogger) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    const {log, authService} = this;

    const accessToken = authService.getAccessToken();

    const canActivate = !!(accessToken && accessToken.id);
    log.debug('[Authenticated guard] Can activate', canActivate);

    if (!canActivate) {
      this.router.navigateByUrl('/login');
    }

    return canActivate;

  }


}

