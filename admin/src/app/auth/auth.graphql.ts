import gql from "graphql-tag";

export const requestPasswordReset = gql`
  mutation RequestPasswordReset($email: String!) {
    requestPasswordReset(email: $email)
  }
`;

export const resetPassword = gql`
  mutation ResetPassword($password: String!) {
    resetPassword(password: $password)
  }
`;

export const authenticateWithEmail = gql`
  mutation AuthenticateWithEmail($email: String!, $password: String!) {
    authenticateWithEmail(email: $email, password: $password) {
      id
      expiresIn
    }
  }
`;

