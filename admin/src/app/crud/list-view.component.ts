import {Component, ContentChild, Input, OnInit, TemplateRef} from '@angular/core';
import {ListItemDirective} from '@app/crud/list-item.directive';
import {FormControl, FormGroup} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {Apollo, QueryRef} from 'apollo-angular';
import {deleteUserByIdMutation} from '@app/users/users.graphql';
import {Observable} from 'rxjs';
import {ErrorService} from '@app/error.service';
import {BaseComponent} from '@app/shared/base.component';
import {DocumentNode} from 'graphql';
import {ListHeaderDirective} from '@app/crud/list-header.directive';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-list-view',
  template: `
    <div class="row">
      <div class="col-10">
        <form class="form-inline mb-sm-3" [formGroup]="form">
          <input class="form-control mr-sm-2 w-50" formControlName="query" type="text" placeholder="Search...">
          
          <select *ngIf="publishedSelect" formControlName="state" class="custom-select mr-2" style="min-width: 180px;">
            <option selected [value]="null">Show all</option>
            <option value="PUBLISHED">Published</option>
            <option value="DRAFT">Draft</option>
          </select>
          
          <button class="btn btn-default" (click)="reset()">Reset</button>
        </form>
      </div>
      <div class="col-2">
        <a class="btn btn-primary pull-right" [routerLink]="['./', 'new']">
          <fa-icon icon="plus"></fa-icon>
          Add
        </a>
      </div>
    </div>
    <div class="table-striped table-users-list">
      <table class="table table-hover">
        <thead class="thead-dark">
        <tr class="row">
          <ng-container *ngTemplateOutlet="listHeaderTemplate"></ng-container>
        </tr>
        </thead>
        <tbody>
        <tr class="row" *ngFor="let model of models$ | async">
          
          <ng-container *ngTemplateOutlet="listItemTemplate ; context: { $implicit: model }">
          </ng-container>


          <td class="col-sm-1">
            
            <a *ngIf="detailQuery" 
               class="btn btn-primary btn-sm mr-sm-2"
               (mouseover)="prefetchModel(model)"
               [routerLink]="['./', model.id]">
              <fa-icon icon="edit"></fa-icon>
            </a>
            
            <button *ngIf="deleteUserByIdMutation" class="btn btn-danger btn-sm" (click)="onDelete(model)">
              <fa-icon icon="trash"></fa-icon>
            </button>
          </td>
          
        </tr>
        </tbody>
      </table>

      <ngb-pagination [collectionSize]="count$ | async"
                      [page]="page$ | async"
                      (pageChange)="onPageChange($event)"
                      aria-label="Default pagination">
      </ngb-pagination>

    </div>  
  `
})
export class ListViewComponent extends BaseComponent implements OnInit {

  @Input() listQuery: DocumentNode;
  @Input() detailQuery: DocumentNode;
  @Input() deleteByIdMutation: DocumentNode;

  @Input() countParser: (data: any) => number;
  @Input() modelsParser: (data: any) => any[];

  @Input() publishedSelect: boolean;

  @ContentChild(ListHeaderDirective, { read: TemplateRef }) listHeaderTemplate;
  @ContentChild(ListItemDirective, { read: TemplateRef }) listItemTemplate;

  form: FormGroup;

  models$: Observable<any[]>;

  count$: Observable<number>;
  page$: Observable<number>;

  pageSize = 10;

  query: QueryRef<any>;

  constructor(private readonly errorService: ErrorService,
              private readonly router: Router,
              private readonly apollo: Apollo,
              private readonly route: ActivatedRoute,
              private readonly toastr: ToastrService) {
    super();
  }

  ngOnInit(): void {

    const queryParams = this.route.snapshot.queryParams;

    this.form = new FormGroup({
      query: new FormControl(queryParams.q)
    });

    if (this.publishedSelect) {
      this.form.addControl('state', new FormControl(queryParams.state))
    }

    this.subscription(() => this.form.valueChanges
      .pipe(
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe(formValue => {
        this.mergeQueryParams({
          q: formValue.query,
          state: formValue.state === 'null' ? null : formValue.state
        });
      })
    );

    const params$ = this.route.queryParams
      .pipe(map(this.defaultParams));

    this.page$ = params$
      .pipe(
        map(params => {
          const {offset, limit} = params;
          return Math.floor(offset / limit) + 1;
        })
      );

    params$.subscribe(params => {

      let {offset, limit, q, state} = params;

      if (!this.query) {

        this.query = this.apollo.watchQuery<any[]>({
          query: this.listQuery,
          variables: {offset, limit, q, state},
          fetchResults: true
        });

        const {valueChanges} = this.query;

        this.count$ = valueChanges
          .pipe(map(({data}) => this.countParser(data)));

        this.models$ = this.query.valueChanges
          .pipe(map(({data}) => this.modelsParser(data)));
      }

      this.fetchMore(params);

    });

  }

  defaultParams(params: Params) {

    const offset = params.offset ? +params.offset : 0;
    const limit = params.limit ? +params.limit : 10;
    const q = params.q;
    const state = params.state || 'ALL';

    return {offset, limit, q, state};
  }

  mergeQueryParams(params: Params) {

    this.router.navigate(
      ['.'], {
        relativeTo: this.route,
        queryParamsHandling: 'merge',
        queryParams: params
      });
  }

  fetchMore(params?: any) {

    let variables = params;

    if (!variables) {
      const {queryParams} = this.route.snapshot;
      variables = this.defaultParams(queryParams);
    }

    this.query.fetchMore({
      variables,
      updateQuery: (prev, {fetchMoreResult}) => {
        return fetchMoreResult || [];
      }
    });
  }

  onPageChange(page: number) {

    const newOffset = (page - 1) * this.pageSize;
    const params: Params = {'offset': newOffset};

    this.mergeQueryParams(params);
  }

  reset() {
    this.form.reset();
    this.router.navigate(['.'], {relativeTo: this.route, queryParams: {}});
  }

  prefetchModel(model: any) {

    const {id} = model;

    // prefetch the info we know the detail screen will need

    this.subscription(() => {

      // we need to subscribe to force the query to happen

      return this.apollo.query({
        query: this.detailQuery,
        variables: {id},
        fetchPolicy: 'network-only' // force a cache sync
      }).subscribe();

    });
  }

  onDelete(model: any) {

    const {id} = model;

    this.apollo.mutate({
      mutation: this.deleteByIdMutation,
      variables: {id}
    }).subscribe(
      () => {
        this.toastr.success('Deleted model');
        this.fetchMore();
      },
      (err) => this.errorService.handleError(err)
    );

  }

}
