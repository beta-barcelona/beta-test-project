import {Component} from '@angular/core';
import {settingDetailQuery, settingListQuery} from "./setting.graphql";

@Component({
  selector: 'app-setting-list',
  template: `
    <app-list-view [listQuery]="listQuery"
                   [detailQuery]="detailQuery"
                   [modelsParser]="modelsParser"
                   [countParser]="countParser">

      <ng-container *listHeader>
        <th scope="col" class="col-sm-1">Id</th>
        <th scope="col" class="col-sm-6">Value</th>
        <th scope="col" class="col-sm-2">Created</th>
        <th scope="col" class="col-sm-2">Last Updated</th>
        <th scope="col" class="col-sm-1">Actions</th>
      </ng-container>

      <ng-container *listItem="let model;">
        <td class="col-sm-1">{{ model.id }}</td>
        <td class="col-sm-6">{{ model.value }}</td>
        <td class="col-sm-2">{{ model.createdAt | timeago:live }}</td>
        <td class="col-sm-2">{{ model.updatedAt | timeago:live }}</td>
      </ng-container>

    </app-list-view>
  `
})
export class SettingListComponent {

  listQuery = settingListQuery;
  detailQuery = settingDetailQuery;

  modelsParser(data: any): any[] {
    return data.settings;
  }

  countParser(data: any): number {
    return data.settingsCount;
  }

}
