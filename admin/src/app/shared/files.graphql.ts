import gql from 'graphql-tag';

export const fileDetailFragment = gql`
  fragment FileDetailView on File {
    id
    mimetype
    filename,
    metadata,
    createdAt,
    updatedAt,
    owner {
        id
    },
    clTransforms
  }
`;

export const updateFileFragment = gql`
  fragment UpdateFileView on File {
    mimetype
    filename,
    metadata,
    clTransforms
  }
`;

export const fileDetailQuery = gql`
  query FileDetail($id: ID!) {
    file(id: $id) {
      ...FileDetailView
    }  
  }
  ${fileDetailFragment}
`;

export const fileByIdQuery = gql`
  query FileById($id: ID!) {
    file(id: $id) {
      ...FileDetailView
    }
  }
  ${fileDetailFragment}
`;

export const updateFileByIdMutation = gql`
  mutation UpdateFileById($id: ID!, $file: UpsertFile) {
    updateFileById(id: $id, file: $file) {
      ...FileDetailView
    }
  }
  ${fileDetailFragment}
`;
