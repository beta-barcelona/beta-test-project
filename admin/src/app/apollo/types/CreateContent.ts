/* tslint:disable */
// This file was automatically generated and should not be edited.

import { UpsertContent } from "./globalTypes";

// ====================================================
// GraphQL mutation operation: CreateContent
// ====================================================

export interface CreateContent_createContent_contentType {
  __typename: "ContentType";
  id: string | null;
  name: string | null;
  slug: string | null;
  formConfig: any | null;
}

export interface CreateContent_createContent {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  data: any | null;
  publishedAt: any | null;
  contentType: CreateContent_createContent_contentType | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface CreateContent {
  createContent: CreateContent_createContent;
}

export interface CreateContentVariables {
  content?: UpsertContent | null;
}
