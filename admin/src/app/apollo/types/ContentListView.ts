/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ContentListView
// ====================================================

export interface ContentListView_contentType {
  __typename: "ContentType";
  id: string | null;
  name: string | null;
}

export interface ContentListView {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  data: any | null;
  publishedAt: any | null;
  contentType: ContentListView_contentType | null;
  createdAt: any | null;
  updatedAt: any | null;
}
