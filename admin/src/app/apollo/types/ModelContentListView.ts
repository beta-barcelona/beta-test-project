/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ModelContentListView
// ====================================================

export interface ModelContentListView_modelContent {
  __typename: "ModelContent";
  name: string | null;
  email: string | null;
}

export interface ModelContentListView {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  publishedAt: any | null;
  modelContent: ModelContentListView_modelContent | null;
  createdAt: any | null;
  updatedAt: any | null;
}
