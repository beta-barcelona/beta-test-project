/* tslint:disable */
// This file was automatically generated and should not be edited.

import { UpsertContent } from "./globalTypes";

// ====================================================
// GraphQL mutation operation: UpdateContentById
// ====================================================

export interface UpdateContentById_updateContentById_contentType {
  __typename: "ContentType";
  id: string | null;
  name: string | null;
  slug: string | null;
  formConfig: any | null;
}

export interface UpdateContentById_updateContentById {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  data: any | null;
  publishedAt: any | null;
  contentType: UpdateContentById_updateContentById_contentType | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface UpdateContentById {
  updateContentById: UpdateContentById_updateContentById;
}

export interface UpdateContentByIdVariables {
  id: string;
  content?: UpsertContent | null;
}
