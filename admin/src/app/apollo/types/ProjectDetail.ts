/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ProjectDetail
// ====================================================

export interface ProjectDetail_content_contentType {
  __typename: "ContentType";
  id: string | null;
  name: string | null;
  slug: string | null;
  formConfig: any | null;
}

export interface ProjectDetail_content {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  data: any | null;
  contentType: ProjectDetail_content_contentType | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface ProjectDetail_contentType {
  __typename: "ContentType";
  id: string | null;
  name: string | null;
  slug: string | null;
  formConfig: any | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface ProjectDetail {
  content: ProjectDetail_content | null;
  contentType: ProjectDetail_contentType | null;
}

export interface ProjectDetailVariables {
  id: string;
}
