import {Component} from '@angular/core';
import {contentTypeListQuery} from './content-type.graphql';

@Component({
  selector: 'app-content-types-list',
  template: `
    <app-list-view [listQuery]="listQuery"
                   [modelsParser]="modelsParser"
                   [countParser]="countParser">

      <ng-container *listHeader>
        <th scope="col" class="col-sm-1">Id</th>
        <th scope="col" class="col-sm-3">Name</th>
        <th scope="col" class="col-sm-3">Slug</th>
        <th scope="col" class="col-sm-2">Created</th>
        <th scope="col" class="col-sm-2">Last Updated</th>
        <th scope="col" class="col-sm-1">Actions</th>
      </ng-container>

      <ng-container *listItem="let model;">
        <td class="col-sm-1">{{ model.id }}</td>
        <td class="col-sm-3">{{ model.name }}</td>
        <td class="col-sm-3">{{ model.slug || '-' }}</td>
        <td class="col-sm-2">{{ model.createdAt | timeago:live }}</td>
        <td class="col-sm-2">{{ model.updatedAt | timeago:live }}</td>
      </ng-container>

    </app-list-view>
  `
})
export class ContentTypeListComponent {

  listQuery = contentTypeListQuery;

  modelsParser(data: any): any[] {
    return data.contentTypes;
  }

  countParser(data: any): number {
    return data.contentTypesCount;
  }

}
