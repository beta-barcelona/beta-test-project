import { MainComponent } from '../layout/main.component';
import { AuthenticatedGuard } from '../auth/guards/authenticated.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContentDetailComponent } from './content-detail.component';
import { ModelListComponent } from '@app/content/model-list.component';
import { UserDetailComponent } from '@app/users/user-detail.component';
import { UsersListComponent } from '@app/users/users-list.component';
import { ImageCropperComponent } from '@app/content/image-cropper.component';

export const contentRoutes: Routes = [
  {
    path: 'content',
    component: MainComponent,
    canActivate: [AuthenticatedGuard],
    children: [
      {
        path: 'models',
        data: {
          title: 'Models'
        },
        children: [
          {
            path: ':id',
            component: ContentDetailComponent,
            data: {
              title: 'Model detail',
              contentType: 'model'
            }
          },
          {
            path: '',
            component: ModelListComponent,
          }
        ]
      },
      {
        path: 'cropper',
        children: [
          {
            path: ':id',
            component: ImageCropperComponent,
            data: {
              title: 'Image cropper',
              contentType: 'file'
            }
          },
        ]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(contentRoutes)],
  exports: [RouterModule]
})
export class ContentRoutesModule { }
