import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CropperComponent } from 'angular-cropperjs';
import { Observable } from 'rxjs/Observable';
import { Location } from '@angular/common';
import { filter, flatMap, map, tap } from 'rxjs/operators';
import { fileByIdQuery, updateFileByIdMutation, fileDetailQuery } from '../shared/files.graphql';
import { Apollo } from 'apollo-angular';
import { ApolloError } from 'apollo-client';
import { ErrorService } from '@app/error.service';
import { cloneDeep } from 'apollo-utilities';
import { ToastrService } from 'ngx-toastr';
import { FileDetailView } from '@app/apollo/types/FileDetailView';
import { FileById } from '../apollo/types/FileById';
// import { ContentDetail, ContentDetail_contentAdmin_contentType } from '@app/apollo/types/ContentDetail';
// import { contentTypeListQuery } from '@app/content/content-type.graphql';
// import { ContentTypeList } from '@app/apollo/types/ContentTypeList';
import { BaseComponent } from '@app/shared/base.component';
import { combineLatest } from 'rxjs';
import { FetchResult } from 'apollo-link';
import { FileDetail } from '@app/apollo/types/FileDetail';

@Component({
  selector: 'app-image-cropper',
  template: `
    <div class="main-cropper-wrapper">
    <div class="wrapper-buttons">  
    <button class="btn btn-primary" type="button" (click)="crop()">Crop Image</button>
    <button class="btn btn-primary" type="button" (click)="reset()">Undo Crop</button>
    </div>
    <angular-cropper [cropperOptions]="config" #image [imageUrl]="secure_url"></angular-cropper>
    <hr>
    <div class="save-wrapper">
      <button class="btn btn-primary mb-3" (click)="onSave()">
        <fa-icon class="ng-fa-icon" fixedwidth="true" icon="save" ng-reflect-icon-prop="save" ng-reflect-fixed-width="true">
        </fa-icon> Save 
      </button>
    </div>
    <h3>Preview: </h3>
      <img class="cropper-preview" [src]="transformed_secure_url">
    </div>
          `,
  styleUrls: ['image-cropper.component.scss']
})
export class ImageCropperComponent extends BaseComponent implements OnInit {

  model$: Observable<FileDetailView>;

  @ViewChild('image') image: CropperComponent;

  config = {
    // viewMode: 3,
    dragMode: 'move',
    autoCropArea: 0.9,
    restore: false,
    modal: true,
    guides: false,
    highlight: true,
    cropBoxMovable: true,
    cropBoxResizable: true,
    toggleDragModeOnDblclick: false,
  };

  urlFirstPart: string;
  transformString: string;
  urlLastPart: string;

  secure_url: string = '';
  transformed_secure_url: string = '';

  file: FileDetailView;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly apollo: Apollo,
    private readonly location: Location,
    private readonly toastrService: ToastrService,
    private readonly errorService: ErrorService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.model$ = this.route.params
      .pipe(
        flatMap(params => {

          const { id } = params;

          return this.apollo
            .query<FileDetail>({
              query: fileDetailQuery,
              variables: { id }
            })
            .pipe(map(({ data }) => {
              const { file } = data;
              // apollo freezes objects so we need to create a mutable copy
              return cloneDeep(file);
            }));


        }),
        tap(file => file)
      );

    this.model$.subscribe(file => {
      const secure_url = file.metadata.secure_url;
      const uploadPosition = secure_url.indexOf('upload') + 6;

      this.urlFirstPart = secure_url.substring(0, uploadPosition);
      this.urlLastPart = secure_url.substring(uploadPosition);
      this.secure_url = secure_url;

      const transforms = file.clTransforms || '';
      this.transformed_secure_url = `${this.urlFirstPart}${transforms}${this.urlLastPart}`;

      this.file = file;
    });

  }

  crop = () => {
    const cropped = this.image.cropper.getData();

    let { width, height, x, y } = cropped;

    width = Math.round(cropped.width);
    height = Math.round(cropped.height);
    x = Math.round(cropped.x);
    y = Math.round(cropped.y);

    this.transformed_secure_url = this.buildTransformedUrl(width, height, x, y);
  }

  buildTransformedUrl(width, height, x, y) {
    this.transformString = `/c_crop,w_${width},h_${height},x_${x},y_${y}`;
    return `${this.urlFirstPart}${this.transformString}${this.urlLastPart}`;
  }

  reset = () => {
    this.image.cropper.reset();
    this.transformed_secure_url = this.secure_url;
    this.transformString = '';
  }

  onSave() {
    let file = { clTransforms: this.transformString };
    // console.log(file);
    // return;

    let observable: Observable<FetchResult<FileDetailView>>;

    observable = this.apollo
      .mutate<FileDetailView>({
        mutation: updateFileByIdMutation,
        variables: {
          id: this.file.id,
          file: file
        }
      });

    this.subscription(() => observable
      .subscribe(
        () => this.location.back(),
        (err) => {
          if (err instanceof ApolloError) {
            // TODO improve error handling
            console.log(err);
          } else {
            this.errorService.handleError(err);
          }
        }
      )
    );

  }

}