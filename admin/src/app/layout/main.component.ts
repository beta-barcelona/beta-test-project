import {Component, HostBinding, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Data, NavigationEnd, Router, RouterOutlet} from '@angular/router';
import {Subscription} from 'rxjs';
import {filter} from 'rxjs/operators';
import {animate, group, query, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-main',
  template: `
    <app-navbar></app-navbar>
    <div class="container-fluid">
      <div class="row">

        <app-sidebar></app-sidebar>

        <main class="col-md-9 ml-sm-auto col-lg-10 px-4" role="main">

          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">{{ title }}</h1>
            <router-outlet name="toolbar"></router-outlet>
          </div>

          <router-outlet></router-outlet>

        </main>

      </div>

    </div>`

})
export class MainComponent implements OnInit, OnDestroy {

  title: string;

  private readonly subs: Subscription[] = [];

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
  }
  
  ngOnInit(): void {

    this.subs.push(
      this.router.events
        .pipe(filter(event => event instanceof NavigationEnd))
        .subscribe((event: NavigationEnd) => {
          this.title = this.determineTitle(this.router.routerState.snapshot.root);
        })
    );

    this.title = this.determineTitle(this.activatedRoute.snapshot.root);
  }

  private determineTitle(route: ActivatedRouteSnapshot, title: string = null) {
    return route.firstChild
      ? this.determineTitle(route.firstChild, route.data.title || title)
      : route.data.title || title;

  }

  ngOnDestroy(): void {
    this.subs.forEach(s => s.unsubscribe());
  }


}
