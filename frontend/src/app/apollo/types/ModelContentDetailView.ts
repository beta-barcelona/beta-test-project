/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ModelContentDetailView
// ====================================================

export interface ModelContentDetailView_modelContent_images {
  __typename: "File";
  id: string | null;
  mimetype: string | null;
  filename: string | null;
  metadata: any | null;
}

export interface ModelContentDetailView_modelContent {
  __typename: "ModelContent";
  name: string | null;
  email: string | null;
  gender: string | null;
  country: string | null;
  inTown: boolean | null;
  height: number | null;
  bust: number | null;
  waist: number | null;
  hips: number | null;
  shoes: number | null;
  eyeColor: string | null;
  hairColor: string | null;
  instagram: string | null;
  images: (ModelContentDetailView_modelContent_images | null)[] | null;
}

export interface ModelContentDetailView {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  modelContent: ModelContentDetailView_modelContent | null;
  createdAt: any | null;
  updatedAt: any | null;
}
