/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SubmittedFormView
// ====================================================

export interface SubmittedFormView {
  __typename: "FormSubmission";
  id: string | null;
  data: any | null;
}
