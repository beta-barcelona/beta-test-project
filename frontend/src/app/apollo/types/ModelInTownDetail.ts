/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ModelInTownDetail
// ====================================================

export interface ModelInTownDetail_modelContent_images {
  __typename: "File";
  id: string | null;
  metadata: any | null;
}

export interface ModelInTownDetail_modelContent {
  __typename: "ModelContent";
  name: string | null;
  country: string | null;
  height: number | null;
  bust: number | null;
  waist: number | null;
  hips: number | null;
  shoes: number | null;
  eyeColor: string | null;
  hairColor: string | null;
  instagram: string | null;
  images: (ModelInTownDetail_modelContent_images | null)[] | null;
}

export interface ModelInTownDetail {
  __typename: "Content";
  id: string | null;
  slug: string | null;
  modelContent: ModelInTownDetail_modelContent | null;
}
