/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: BasicContentDetail
// ====================================================

export interface BasicContentDetail_content_contentType {
  __typename: "ContentType";
  name: string | null;
}

export interface BasicContentDetail_content_basicContent {
  __typename: "BasicContent";
  heading: string | null;
  body: string | null;
}

export interface BasicContentDetail_content {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  data: any | null;
  contentType: BasicContentDetail_content_contentType | null;
  basicContent: BasicContentDetail_content_basicContent | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface BasicContentDetail {
  content: BasicContentDetail_content | null;
}

export interface BasicContentDetailVariables {
  id: string;
}
