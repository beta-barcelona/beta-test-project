/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FileForEmailFragment
// ====================================================

export interface FileForEmailFragment {
  __typename: "File";
  id: string | null;
  metadata: any | null;
}
