/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ModelContentDetail
// ====================================================

export interface ModelContentDetail_content_modelContent_images {
  __typename: "File";
  id: string | null;
  mimetype: string | null;
  filename: string | null;
  metadata: any | null;
}

export interface ModelContentDetail_content_modelContent {
  __typename: "ModelContent";
  name: string | null;
  email: string | null;
  gender: string | null;
  country: string | null;
  inTown: boolean | null;
  height: number | null;
  bust: number | null;
  waist: number | null;
  hips: number | null;
  shoes: number | null;
  eyeColor: string | null;
  hairColor: string | null;
  instagram: string | null;
  images: (ModelContentDetail_content_modelContent_images | null)[] | null;
}

export interface ModelContentDetail_content {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  modelContent: ModelContentDetail_content_modelContent | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface ModelContentDetail {
  content: ModelContentDetail_content | null;
}

export interface ModelContentDetailVariables {
  id: string;
}
