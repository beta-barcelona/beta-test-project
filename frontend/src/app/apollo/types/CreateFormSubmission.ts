/* tslint:disable */
// This file was automatically generated and should not be edited.

import { UpsertFormSubmission } from "./globalTypes";

// ====================================================
// GraphQL mutation operation: CreateFormSubmission
// ====================================================

export interface CreateFormSubmission_createFormSubmission {
  __typename: "FormSubmission";
  id: string | null;
  data: any | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface CreateFormSubmission {
  createFormSubmission: CreateFormSubmission_createFormSubmission;
}

export interface CreateFormSubmissionVariables {
  formSubmission?: UpsertFormSubmission | null;
}
