/* tslint:disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export interface UpsertFormSubmission {
  data?: any | null;
  formConfigId?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
