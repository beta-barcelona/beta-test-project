/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ContentByIdView
// ====================================================

export interface ContentByIdView {
  __typename: "Content";
  title: string | null;
  meta: any | null;
  data: any | null;
}
