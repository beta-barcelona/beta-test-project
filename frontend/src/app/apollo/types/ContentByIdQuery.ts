/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ContentByIdQuery
// ====================================================

export interface ContentByIdQuery_content {
  __typename: "Content";
  title: string | null;
  meta: any | null;
  data: any | null;
}

export interface ContentByIdQuery {
  content: ContentByIdQuery_content | null;
}

export interface ContentByIdQueryVariables {
  id: string;
}
