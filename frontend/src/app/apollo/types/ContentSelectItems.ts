/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ContentSelectItems
// ====================================================

export interface ContentSelectItems_contents {
  __typename: "Content";
  id: string | null;
  title: string | null;
  slug: string | null;
}

export interface ContentSelectItems {
  contents: (ContentSelectItems_contents | null)[];
}

export interface ContentSelectItemsVariables {
  contentType?: string | null;
}
