/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: AboutContentDetail
// ====================================================

export interface AboutContentDetail_content_contentType {
  __typename: "ContentType";
  name: string | null;
}

export interface AboutContentDetail_content_aboutContent_people {
  __typename: "Person";
  name: string | null;
  position: string | null;
  email: string | null;
}

export interface AboutContentDetail_content_aboutContent_coverImageFile {
  __typename: "File";
  id: string | null;
  mimetype: string | null;
  filename: string | null;
  metadata: any | null;
}

export interface AboutContentDetail_content_aboutContent {
  __typename: "AboutContent";
  heading: string | null;
  people: (AboutContentDetail_content_aboutContent_people | null)[] | null;
  coverImageFile: AboutContentDetail_content_aboutContent_coverImageFile | null;
}

export interface AboutContentDetail_content {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  data: any | null;
  contentType: AboutContentDetail_content_contentType | null;
  aboutContent: AboutContentDetail_content_aboutContent | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface AboutContentDetail {
  content: AboutContentDetail_content | null;
}

export interface AboutContentDetailVariables {
  id: string;
}
