import gql from 'graphql-tag';

export const routeFragment = gql`
  fragment Route on Content {
    id
    slug
    contentType {
      name,
      slug
    }
  }
`;

export const initialConfigQuery = gql`
  query InitialConfig {
    contents(limit: 1000) {
      ...Route
    },
    settings(limit: 1000) {
      id
      value
    }
  }
  ${routeFragment}
`;

export const modelsInTownFragment = gql`
  fragment ModelInTownDetail on Content {
    id
    slug
    modelContent {
      name
      country
      height
      bust
      waist
      hips
      shoes
      eyeColor
      hairColor
      instagram
      images {
        id
        metadata
      }
    }
  }
`;

export const modelContentDetailFragment = gql`
  fragment ModelContentDetailView on Content {
    id
    title
    meta
    slug
    modelContent {
      name
      email
      gender
      country
      inTown
      height
      bust
      waist
      hips
      shoes
      eyeColor
      hairColor
      instagram
      images {
        id
        mimetype
        filename
        metadata
      }
    }
    createdAt
    updatedAt
  }
`;

export const formConfigFragment = gql`
  fragment FormConfigView on FormConfig {
    id
    name
    config
    createdAt
    updatedAt
  }

`;

export const contentByIdView = gql`
  fragment ContentByIdView on Content {
    title
    meta
    data
  }
`;

export const contentSelectView = gql`
  fragment ContentSelectView on Content {
    id
    title
    slug
  }
`;

export const formContentView = gql`
  fragment FormContentView on Content {
    title
    meta
    data
    formContent {
      title
      subtitle
      buttonText
      submitted
    }
  }
`;

export const modelContentDetailQuery = gql`
  query ModelContentDetail($id: ID!) {
    content(id: $id) {
      ...ModelContentDetailView
    }
  }
  ${modelContentDetailFragment}
`;


export const modelsInTownQuery = gql`
  query ModelsInTownQuery($id: ID!) {
    content(id: $id){
      title
      meta
    }
    modelsInTown {
      ...ModelInTownDetail
    }
  }
  ${modelsInTownFragment}
`;

export const formContentByIdQuery = gql`
  query FormContentByIdQuery($id: ID!) {
    content(id: $id) {
      ...FormContentView
    }
  }
  ${formContentView}
`;

export const contentByIdQuery = gql`
  query ContentByIdQuery($id: ID!) {
    content(id: $id) {
      ...ContentByIdView
    }
  }
  ${contentByIdView}
`;

export const modelsByGenderQuery = gql`
  query ModelsByGenderQuery($gender: String!, $firstLetter: String) {
    modelsByGender(gender: $gender, firstLetter: $firstLetter) {
      ...ModelContentDetailView
    }
  }
  ${modelContentDetailFragment}
`;

export const selectedModelsQuery = gql`
  query SelectedModelsQuery($ids: [ID]!) {
    selectedModels(ids: $ids) {
      ...ModelContentDetailView
    }
  }
  ${modelContentDetailFragment}
`;

export const contentSelectItemsQuery = gql`
  query ContentSelectItems($contentType: String) {
    contents(contentType: $contentType) {
      ...ContentSelectView
    }
  }
  ${contentSelectView}
`;

export const formConfigByIdQuery = gql`
  query FormConfigByIdQuery($id: ID!) {
    formConfig(id: $id) {
      ...FormConfigView
    }
  }
  ${formConfigFragment}
`;

export const fileDetailFragment = gql`
  fragment FileDetailView on File {
    id
    mimetype
    filename,
    metadata,
    createdAt,
    updatedAt,
    owner {
      id
    }
  }
`;

export const fileForEmailFragment = gql`
  fragment FileForEmailFragment on File {
    id
    metadata
  }
`;

export const fileByIdQuery = gql`
  query FileById($id: ID!) {
    file(id: $id) {
      ...FileDetailView
    }
  }
  ${fileDetailFragment}
`;

export const filesForEmailQuery = gql`
  query Files($ids: [ID]!) {
    files(ids: $ids) {
      ...FileForEmailFragment
    }
  }
  ${fileForEmailFragment}
`;

export const formSubmissionFragment = gql`
  fragment FormSubmissionView on FormSubmission {
    id,
    data,
    createdAt,
    updatedAt
  }
`;

export const createFormSubmissionMutation = gql`
  mutation CreateFormSubmission($formSubmission: UpsertFormSubmission) {
    createFormSubmission(formSubmission: $formSubmission) {
      ...FormSubmissionView
    }
  }
  ${formSubmissionFragment}
`;

export const settingDetailFragment = gql`
  fragment SettingDetailView on Setting {
    id
    value
    createdAt
    updatedAt
  }
`;

export const settingListQuery = gql`
  query SettingList($offset: Int, $limit: Int, $q: String){
    settings(offset: $offset, limit: $limit, q: $q) {
      ...SettingDetailView
    }
    settingsCount(q: $q)
  }
  ${settingDetailFragment}
`;

export const loadOldDataMutation = gql`
  mutation LoadOldDataMutation($input: String) {
    loadOldData(input: $input) {
      title
      alias
      short_desc
      descr
      image
      gallery_id
      manufacturer_id
      metakey
      metadesc
      ordering
      created
      published
      checked_out
      checked_out_time
      recommended
      hits
      default_image
      in_town
      new_face
      height
      bust
      waist
      hips
      shoe_size
      hair_colour
      eye_colour
      race
      pdf
    }
  }
`;
