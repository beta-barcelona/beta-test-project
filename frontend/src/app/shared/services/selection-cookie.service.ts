import {EventEmitter, Injectable} from "@angular/core";
import {CookieService} from "ngx-cookie-service";
import {ToastrService} from "ngx-toastr";

@Injectable()
export class SelectionCookieService {

  selectionChanged: EventEmitter<number> = new EventEmitter();

  constructor(private cookieService: CookieService, private toastr: ToastrService) {

  }

  getSelectedIds(): string[] {

    const cookie = this.cookieService.get('selectedIds');
    if (!cookie) return [];

    const data = JSON.parse(cookie);

    if (!data.ids) {
      this.toastr.error('Selected model ids not found in cookie.', 'Error');
    }

    return data.ids;

  }

  addSelectedId(id) {
    const ids = this.getSelectedIds();

    if (ids.indexOf(id) > -1) return;

    ids.push(id);
    const cookie = JSON.stringify({ids: ids});
    this.cookieService.set('selectedIds', cookie, 30);

    // Emit event to inform listeners selected ids have changed
    this.selectionChanged.next(ids.length);
  }

  removeSelectedId(id) {
    const ids = this.getSelectedIds();
    const idx = ids.indexOf(id);

    if (idx < 0) return;

    ids.splice(idx, 1);

    const cookie = JSON.stringify({ids: ids});
    this.cookieService.set('selectedIds', cookie, 30);

    // Emit event to inform listeners selected ids have changed
    this.selectionChanged.next(ids.length);
  }

}
