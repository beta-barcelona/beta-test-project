import { Component, Inject } from '@angular/core';
import { WINDOW } from '@app/window.service';

@Component({
  selector: 'scroll-up',
  template: `
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="52"
      height="52"
      viewBox="0 0 52 52"
      (click)="onClick()"
      class="scroll-up"
    >
      <g fill="none" fill-rule="evenodd">
        <path
          fill="#000"
          fill-rule="nonzero"
          d="M41.2 26.008l-9-4.5v9l9-4.5zm-28.5.5h20.5v-1h-21v1h.5z"
        />
        <circle cx="26" cy="26" r="24" stroke="#000" />
      </g>
    </svg>
  `,
  styles: [
    `
      :host {
        display: block;
        text-align: center;
        margin: 70px 0;
      }
      .scroll-up {
        text-align: center;
        cursor: pointer;
        transform: rotate(270deg);
        /* z-index: -1;*/
        /* position: relative; */
      }
    `,
  ],
})
export class ScrollUpComponent {
  constructor(@Inject(WINDOW) private readonly window: Window) {}

  onClick() {
    window.scroll({
      top: 0,
      behavior: 'smooth',
    });
  }
}
