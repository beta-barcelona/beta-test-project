import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'legal-modal',
  template: `
    <div class="legal-modal"><ng-content></ng-content><div [innerHTML]="text"></div></div>
  `,
  styleUrls: ['./legal-modal.component.sass'],
})
export class LegalModalComponent implements OnInit {
  @Input() legalText: string;
  text: string;

  ngOnInit() {
    switch (this.legalText) {
      case 'legal':
        this.text = this.legalWarning;
        break;
      case 'cookies':
        this.text = this.cookiesPolicy;
        break;
      case 'privacy':
        this.text = this.privacyPolicy;
        break;
    }
  }


  legalWarning = `<h2>AVISO LEGAL</h2>
  <p class="small">Fecha de la elaboración del presente aviso legal: 29 /01 / 2019</p>
  
  <h3>1. DATOS IDENTIFICATIVOS</h3>
  
  <p>En cumplimiento con el deber de información recogido en el artículo 10 de la Ley 34/2002 de 11 de julio, de Servicios de la Sociedad de la Información y el Comercio electrónico, a continuación se reflejan los siguientes datos:</p>
  <ul>
      <li>La sociedad titular y propietaria de este sitio web es: BERTA MODELS MANAGEMENT, S.L.</li>
      <li>CIF: B-65555880	</li>
      <li>Domicilio en C/ Monterols, 6, 08034, Barcelona, España.</li>
      <li>Buzón/Email de contacto: berta@bertamodels.com </li>
  </ul>

  <h3>2. PROPIEDAD INTELECTUAL</h3>
  
  <p>El presente sitio web, incluyendo el código fuente, logotipos y los contenidos elaborados por su titular o sus colaboradores, están protegidos por la normativa nacional e internacional vigente sobre propiedad intelectual, encontrándose todos los derechos reservados, de forma que el usuario se encuentra facultado únicamente para  efectuar la navegación a través del website para su visualización como uso privado.</p>
  <p>El usuario se compromete a respetar todos los derechos de “propiedad intelectual e industrial” asociados al presente sitio web. Podrá visualizar los elementos del sitio web e incluso imprimirlos, copiarlos y almacenarlos en el disco duro de su ordenador o en cualquier otro soporte físico siempre y cuando sea, única y exclusivamente, para su uso personal y privado, quedando, por tanto, terminantemente prohibida la transformación, distribución, comunicación pública, puesta a disposición o cualquier otra forma de explotación, así como su modificación, alteración o descompilación. El usuario deberá abstenerse de suprimir, alterar, eludir o manipular cualquier dispositivo de protección o sistema de seguridad que estuviera instalado en el las páginas de BERTA MODELS MANAGEMENT, S.L. </p>
  
  <h3>3. EXCLUSIÓN DE GARANTÍAS Y RESPONSABILIDAD</h3>
  <p>El propietario del sitio web y sus licenciantes o mandatarios no se hacen responsables, en ningún caso, de los daños y perjuicios de cualquier naturaleza que pudieran ocasionar, a título enunciativo: errores u omisiones en los contenidos, falta de disponibilidad del sitio web o la transmisión de virus o programas maliciosos o lesivos en los contenidos, a pesar de haber adoptado todas las medidas tecnológicas necesarias para evitarlo. </p>
  <p>El propietario del sitio web no se responsabiliza de los enlaces derivados de su website informado al usuario que cuando se encuentre en otro dominio diferente al que se encuentra. Por favor, lea la política de privacidad y aviso legal que determinen las condiciones de uso de dicho sitio web.</p>
  <h3>4. MODIFICACIONES</h3>
  <p>El propietario del sitio web se reserva el derecho de efectuar sin previo aviso las modificaciones que considere oportunas en este website, pudiendo cambiar, suprimir o añadir tanto los contenidos y servicios que se presten a través de la misma como la forma en la que éstos aparezcan presentados o localizados en el sitio web y el acceso a estos. </p>
  <h3>5. LEGISLACIÓN APLICABLE Y JURISDICCIÓN</h3>
  <p>La relación entre el propietario del sitio web y el usuario se regirá por la normativa española vigente y cualquier controversia se someterá a los Juzgados y tribunales de la ciudad de Barcelona.</p>`;

  cookiesPolicy = `<h2>POLITICA DE COOKIES</h2> 
  <p class="small">Fecha de la elaboración de la presente política de cookies: 29/ 01/ 2019</p>
  
  <h3>1. Introducción</h3>
  <p>El sitio web podrá utilizar cookies propias y/o de terceros para mejorar la experiencia del usuario y ofrecer contenidos orientados a sus intereses. La utilización del procedimiento de registro en el sitio web y/o la navegación (con el navegador habilitado para la aceptación de cookies), el usuario acepta la instalación de cookies, salvo que se oponga, según se detalla en esta Política de Cookies.</p>
  <p>La Política de Cookies está sujeta a actualizaciones periódicas. El objeto, es ayudarle a comprender el uso que se realiza de las cookies, la finalidad, así como las diferentes posibilidades que tiene el usuario para su gestión.</p>
  <p>Los usuarios pueden acceder a esta información en cualquier momento a través del link habilitado en el sitio web. De la misma manera, podrán modificar sus preferencias sobre la aceptación de cookies a través de las opciones que tiene establecidas su navegador.</p>
  <p>La Política de Cookies del sitio web comprende el dominio www.bertamodels.com</p>
  <h3>2. Definición de una Cookie</h3>
  <p>Una “Cookie” es un pequeño archivo de texto que un sitio web guarda en el navegador del usuario. Las cookies facilitan el uso y la navegación por una página web y son esenciales para el funcionamiento de internet, aportando innumerables ventajas en la prestación de servicios interactivos.</p>
  <p>Las cookies se utilizan, por ejemplo, para gestionar la sesión del usuario (reduciendo el número de veces que tiene que incluir su contraseña), mejorar los servicios ofrecidos, o para adecuar los contenidos de una página web a sus preferencias.</p>
  <p>Las cookies pueden ser de “sesión”, por lo que se borrarán una vez el usuario abandone la página web que las generó o “persistentes”, que permanecen en su ordenador hasta una fecha determinada.</p>
  <p>Asimismo las Cookies pueden ser “propias”, gestionadas por el dominio al que el usuario accede y del que solicita un determinado servicio (en este caso los dominios titularidad de BERTA MODELS MANAGEMENT, S.L.) o “cookies de terceros”, enviadas al equipo de usuario desde un dominio diferente al que se accede.</p>
  
  <h3>3. Clasificación de las Cookies que pueden darse en el sitio web:</h3>
      <h4>1. Cookies Analíticas</h4>
  <p>El sitio web podrá utilizar las denominadas  “cookies analíticas” para recabar datos estadísticos sobre la actividad del usuario al visitar la página, así como la actividad general de la misma. La información recogida es anónima y permite optimizar la navegación del sitio web y garantizar la calidad de servicio al usuario. Se podrán utilizar cookies propias y/o de para el análisis de nuestra página. El usuario podrá excluir su actividad mediante los sistemas de exclusión facilitados por las herramientas analíticas de su navegador.</p>
      <h4>2. Cookies de uso interno</h4>
  <p>El sitio web podrá utiliza las denominadas “cookies de uso interno” para el funcionamiento del sitio web. En concreto este sitio web utiliza cookies para: (1) permitir la autenticación del usuario; (2) el mantenimiento de la sesión cuando navega por la página; (3) la contratación de un producto o servicio; (4) establecer preferencias asociadas al idioma y seleccionadas por el usuario o de gestión para el acceso del usuario al centro de soporte. La desactivación de estas cookies, mediante el bloqueo de las mismas en las opciones del navegador, puede impedir el funcionamiento correcto de algunas de las funcionalidades del presente sitio web.</p>
      <h4>3. Cookies usadas por redes sociales</h4>
  <p>Este sitio web podrá implementar funcionalidad asociadas al acceso de icono de  redes sociales que ofrecen la posibilidad al usuario compartir contenidos de su interés. Las cookies utilizadas por determinadas redes sociales evitan que el usuario tenga que dar nuevamente sus datos para acceder a dichos contenidos. Dichas funcionalidades (asociadas a iconos) no tienen acceso a los datos introducidos por los usuarios al compartir un determinado contenido en una red social.</p>
  <h3>4. Finalidad de cada una de las cookies utilizadas</h3>
  <p>A continuación, detallamos las cookies utilizadas en nuestra web y la finalidad de las mismas. Relación de cookies utilizadas en el dominio bertamodels.com
  <p>Denominación de las cookies:</p>
  <p>Finalidad:</p>
  <h3>5. Procedimientos para deshabilitar, bloquear y/o  eliminar cookies</h3>
  <p>El usuario podrá permitir, bloquear o eliminar las cookies instaladas en su equipo a través de la configuración de las opciones del navegador que utilizas para tal efecto. En caso de que no permita la instalación de cookies en su navegador es posible que no pueda acceder a algunos de los servicios y que su experiencia en el sitio web pueda resultar menos satisfactoria. En los siguientes enlaces tiene a su disposición toda la información para configurar o deshabilitar las cookies en cada navegador:</p>
  <p>
    <ul>
      <li><a href="https://support.google.com/chrome/answer/95647?hl=es" target="_blank">Google Chrome</a></li>
      <li><a href="https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we" target="_blank">Mozilla Firefox</a></li>
      <li><a href="http://windows.microsoft.com/es-es/windows-vista/block-or-allow-cookies" target="_blank">Internet Explorer</a></li>
      <li><a href="http://support.apple.com/kb/ht1677?viewlocale=es_es" target="_blank">Safari para IOS (iPhone yiPad)</a></li>
      <li><a href="https://support.google.com/chrome/answer/2392971?hl=es" target="_blank">Chrome para Android</a></li>
      <li>Otros: para otros navegadores no referenciados, mediante la inserción en el navegador de uso de su pc/portátil o similares, ponga la siguiente frase “deshabilitar cookies + nombre del navegador”, tendrá acceso a link de ayuda propuesto por el propio navegador al respecto, para tal efecto.</li>
    </ul>
  </p>
  <h3>6. Consentimiento</h3>
  <p>En BERTA MODELS MANAGEMENT, S.L., nunca se guardan los datos personales de los usuarios, a excepción de la dirección IP de acuerdo a lo descrito anteriormente, salvo que quieras registrarte, de forma voluntaria, con el fin de poder solicitar cita previa.</p>
  <p>Mediante el acto de navegación y permanencia en el presente sitio web indica que está consintiendo el uso de las cookies referenciadas, de acuerdo a lascondiciones contenidas en la presente Política de Cookies.</p>`;

  privacyPolicy = `<h2>POLITICA DE PRIVACIDAD – TRATAMIENTO DE DATOS PERSONALES</h2>

  <p class="small"></p>Fecha de la elaboración de la presente política de privacidad: 29/01/2019</p>
  
  <p>BERTA MODELS MANAGEMENT, S.L. (en adelante el propietario del sitio web), de acuerdo con las recomendaciones
    establecidas por la Agencia Española de Protección de Datos, y otros organismos de ámbito autonómico como la Agencia
    Catalana de Protecciò de Dades en relación con dar alcance al derecho de información a Usted como usuario (titular de
    datos), ha elaborado una nueva política de privacidad:</p>
  
  <h3>POLITICA DE PRIVACIDAD</h3>
  
  <h4>1.1 Responsable del Tratamiento</h4>
  
  <p>Estimado Usuario, el responsable de tratamiento y recogida de datos a través del presente sitio web va a ser la
    empresa.
    Así pues, tenga en cuenta que cuando Usted entra (navega) y se relaciona a través de la presente página web lo está
    haciendo con BERTA MODELS MANAGEMENT, S.L.</p>
  
  <p>Denominación: BERTA MODELS MANAGEMENT, S.L.<br>
    C.I.F.: B-65555880<br>
    Domicilio: C/ Monterols, 6, 08034, Barcelona, España.<br>
    Email: berta@bertamodels.com</p>
  <h4>1.2 Finalidad del tratamientode los datos</h4>
  <p>En este punto presentamos una descripción de las finalidades del tratamiento de los datos que pueden darse/recogerse
    en
    el presente sitio web, asociada a los ficheros que trata BERTA MODELS MANAGEMENT, S.L.</p>
  
  <p>Fichero: Usuarios web (contactos)<br>
    Finalidad: Navegación (Cookies) o envío de características físicas e imágenes.<br>
    Observaciones: Asociadas a un formulario web</p>
  
  <p><p class="small">[Le rogamos, en su calidad de usuario/titular de datos que antes de aceptar la política de privacidad (clicar
      la casilla de aceptación) o consentimiento para tratamiento de datos, realice un acercamiento a la información
      básica
      con carácter general, y de forma específica a la presente tabla donde se determina la finalidad de uso de los datos
      que
      usted nos proporciona y el fichero o ficheros donde se pueden integrar los mismos (datos).]</p></p>
  
  <h4>1.3 Plazos o criterios de conservación de los datos</h4>
  
  <p>En el presente punto se da alcance a la información relacionada con la finalidad de tratamiento de datos que puede
    darse
    a través del presente sitio web. Establecer que BERTA MODELS MANAGEMENT, S.L., puede tratar más ficheros, pero aquí
    solo
    se explicitan los que tienen impacto con los datos tratados en el sitio web.</p>
  
  <p>Ficheros: Usuarios web (contactos)<br>
    Plazo de conservación: Los datos de contacto se conservarán de forma indefinida mientras que no medie solicitud de
    oposición o cancelación por parte del titular de datos.</p>
  
  <p><p class="small">[El mantenimiento del plazo de los datos se establece en base a un criterio propio de BERTA MODELS
      MANAGEMENT, S.L.,
      asociado a la finalidad de uso de dichos datos, criterios establecidos por una base jurídica que obliga a su
      mantenimiento a BERTA MODELS MANAGEMENT, S.L. (otra ley que exige su mantenimiento) y otros asociados informes
      estadísticos o históricos que pueden ser de interés para el BERTA MODELS MANAGEMENT, S.L., a nivel estratégico o
      gerencial].</p></p>
  
  <h4>1.4 Destinatarios” (de cesiones o transferencias)</h4>
  
  <p>
    Ficheros: Usuarios web<br>
    Cesionarios con carácter general: Organismos públicos competentes para la solicitud derivado de que la misma de
    acuerdo
    al marco normativo, no es necesario el consentimiento de los datos.
  </p>
  
  <h4>1.5 Transferencias Internacionales de Datos</h4>
  
  <p>En este momento BERTA MODELS MANAGEMENT, S.L., en su calidad de responsable de fichero/datos, no realiza cesión o
    trasferencias de datos a terceros (otras empresas) ubicados en un estado diferente de la Unión Europea.</p>
  
  <h4>1.6 Derechos de los titulares de datos(de las personas interesadas)</h4>
  
  <p>Con el objeto de dar alcance a uno de los aspectos fundamentales del actual marco normativo (RGPD y LOPDGDD), como es
    el
    de los derechos atribuidos a las personas físicas (titulares de datos), le acercamos un email: berta@bertamodels.com
    para que pueda solicitarnos, si así lo estima, un formulario para el ejercicio de los derechos que a continuación se
    referencian:</p>
  
  <p>a. Acceso</p>
  
  <p><p class="small">[ Nota: El derecho de acceso es el que tiene un titular de datos, como puede ser su caso, consistente en
      poder conocer
      la información que la sociedad tiene de usted.]</p></p>
  
  <p>b. Rectificación</p>
  
  <p><p class="small">[ Nota: El derecho de rectificación es el que tiene un titular de datos, como puede ser su caso, consistente
      rectificar
      aquella información/datos que la sociedad tiene de un titular de datos, por ejemplo usted, y no se encuentra
      actualizada
      o adecuada a la realidad del titular de datos o es errónea.]</p></p>
  
  <p>c. Cancelación</p>
  
  <p><p class="small">[ Nota: El derecho de cancelación es aquel derecho a solicitar que los datos sean borrados cuando ya no sean
      necesarios
      o si el tratamiento es ilícito]</p></p>
  
  <p>d. Oposición a su tratamiento</p>
  
  <p><p class="small">[ Nota: El derecho de oposición es el que tiene un titular de datos, como puede ser su caso, consistente en
      oponerse al
      uso de aquella información/datos que la sociedad tiene de usted, y usted no quiere que se siga haciendo uso de la
      misma
      (información/datos) para determinadas finalidades específicas, por ejemplo no recibir información
      comercial.]</p></p>
  
  <p>e.Supresión</p>
  
  <p><p class="small">[ Nota: El derecho de supresión es el que tiene un titular de datos, como puede ser su caso, consistente en
      suprimir
      aquella información/datos que la sociedad tiene de usted mismo, y que usted no quiere que se siga haciendo uso de la
      misma (información/datos), amparado en supuestos legales.]</p></p>
  
  <p>f. Portabilidad de sus datos</p>
  
  <p><p class="small">[ Nota: El derecho a recibir los datos personales en un formato de lectura mecánica y enviarlos a otro
      responsable de
      tratamiento.]</p></p>
  
  <p>g. Solicitar la Limitación del tratamiento</p>
  
  <p><p class="small">[ Nota: Solicitar la limitación del tratamiento de sus datos personales en determinados casos]</p></p>
  
  <h4>1.7 Fuente de los datos (origen)</h4>
  
  <p>Con carácter general los datos, a través del presente sitio web, son obtenidos:</p>
  
  <ul>
    <li>Del propio interesado</li>
  </ul>
  
  <p><p class="small">[*Nota: Los datos proporcionadospor un titular (usuario web), Usted, en cualquiera de los formularios o
      procedimientos
      de recogida de datos establecidos para tal efecto en el sitio web, serán los suyos propios, no debiendo realizar,
      con
      carácter general en procesos de registro datos de terceros, salvo que tenga capacidad de representación o
      autorización
      expresa].</p></p>
  
  <h4>1.8 Formularios de recogida de datos</h4>
  
  <p></p>Con carácter general, en el sitio web podemos encontrar los siguientes formularios para la recogida de datos:</p>
  
  <p><strong>Tipo:</strong> Formulario de contacto
    <strong>Finalidad:</strong> Contactar con BERTA MODELS MANAGEMENT, S.L. para enviar información/características de
    modelos.<br>
    <strong>Cesionarios:</strong> No está prevista la cesión de datos a terceros<br>
    <strong>Observaciones:</strong> Se solicitarán datos personales como (nombre, fecha de nacimiento, ciudad, email,
    teléfono, estatura,
    peso, imágenes).</p>
  
  <h4>1.9 Redes Sociales</h4>
  
  <p>BERTA MODELS MANAGEMENT, S.L. podrá utilizar diferentes redes sociales (sitios, páginas o perfiles) para publicitar
    sus
    actividades, en cualquier caso, si decide recopilar o recoger datos de contactos de usuarios para realizar acciones
    directas de prospección comercial, será siempre, cumpliendo con las exigencias legales de la Ley Orgánica de
    Protección
    de Datos (LOPDGDD 3/2018) y Ley de Sociedades de la Información – Comercio Electrónico (LSSICE 34/2002).</p>
  
  <p>Así pues, queremos acercarle que BERTA MODELS MANAGEMENT, S.L., al no ser propietario de las plataformas donde se
    integran las redes sociales (sitios, páginas o perfiles) en las que se hace presente, con carácter general, BERTA
    MODELS
    MANAGEMENT, S.L. y el USUARIO, están supeditados a las condiciones de uso (avisos legales) establecidas por las mismas
    (redes sociales). Así pues, con objeto de dar alcance a su derecho de información detallamos el enlace a la política
    de
    privacidad de las diferentes redes sociales en las que podrá hacerse presente BERTA MODELS MANAGEMENT, S.L.:</p>
  
  <p>Facebook: <a href="https://www.facebook.com/help/323540651073243/"
      target="_blank">https://www.facebook.com/help/323540651073243/</a></p>
  
  <p>Aun así, en aquello que no sea aplicable, no recoja o permita la ampliación de las condiciones de uso de la red
    social,
    y que pueda ser supletorio a las mismas (condiciones de uso) BERTA MODELS MANAGEMENT, S.L. ha elaborado una serie de
    normas específicas al respecto (en el uso de redes sociales) para los usuarios. BERTA MODELS MANAGEMENT, S.L. ha
    creado
    diversas páginas, sitios y perfiles en redes sociales para atender, informar y acercar conocimiento de su actividad.
  </p>
  
  <p>Para que estos sitios, páginas y/o perfiles sean útiles, es necesario que el usuario asuma unas reglas básicas. Así
    pues, recomendamos que lea con atención las siguientes normas que se ha elaborado al respecto: </p>
  
  <ol>
    <li>ecuerde que estos sitios, páginas o perfiles cuentan con foros de carácter público. Por tanto, el hecho de
      publicar
      cualquier dato, comentario o información, constituye que el usuario asume, que puede ser visualizado/accesible por
      el
      resto de usuarios integrados a la red social y por el propio BERTA MODELS MANAGEMENT, S.L. (en su calidad de titular
      y
      administrador del sitio, página y/o perfil).</li>
    <li>e solicita al usuario que trate de aportar valor en las publicaciones. Recordar que el usuario es el
      responsable
      de
      dichas publicaciones. En caso de dudas, aconsejamos no publicar.</li>
    <li>stos sitios, páginas y/o perfiles (asociados a redes sociales) constituyen foros de intercambio de opiniones o
      aportaciones por parte de los usuarios, pero no es el marco apropiado para crear controversia ni descalificar a
      terceros.</li>
    <li>e solicita al usuario un trato de respeto para el conjunto de usuarios integrados en el sitio, página y/o
      perfil.</li>
    <li>e solicita al usuario el uso de un lenguaje apropiado y correcto.</li>
    <li>e solicita al usuario que no publique material publicitario ni haga uso de estos canales para establecer
      acciones de
      carácter lucrativo o comercial.</li>
    <li>stos canales o medios no son los mejores lugares para la promoción de un usuario o la búsqueda de empleo por
      parte
      de éste.</li>
    <li>os datos que se publiquen, por parte de los usuarios, deben ser veraces.</li>
    <li>l usuario, en el caso de compartir datos o informaciones propias (textos, fotografías, gráficos, videos o
      audios)
      queda informado que otorga a BERTA MODELS MANAGEMENT, S.L., la autorización para reproducir en cualquier medio o
      canal
      físico o virtual dichos datos o informaciones.</li>
    <li>Los símbolos o logotipos que se utilizan en estos sitios, páginas o perfiles son marcas propiedad de BERTA
      MODELS
      MANAGEMENT, S.L. (o cuenta con autorización al respecto). También son titularidad de BERTA MODELS MANAGEMENT, S.L.,
      los
      contenidos colgados, por tanto, BERTA MODELS MANAGEMENT, S.L. se reserva todos los derechos de propiedad intelectual
      e
      industrial asociados a los mismos. Usted, como usuario, se compromete a respetarlos y a no utilizarlos sin la debida
      autorización.</li>
    <li>La descarga los contenidos está circunscrita a un uso personal y privado.</li>
    <li>BERTA MODELS MANAGEMENT, S.L., desea preservar el buen uso de estas redes y, por ello, BERTA MODELS
      MANAGEMENT,
      S.L.
      como administrador del sitio, página, perfil y/o recurso de la red social en cuestión, se reserva el derecho a
      eliminar,
      cualquier publicación o comentario que:
      <ol type="a">
        <li>Considere ilegal, calumnioso, irrespetuoso, amenazante, infundado, inapropiado, ética o socialmente
          reprochable
          o
          que, de alguna forma, pueda ocasionar daños y perjuicios materiales o morales a BERTA MODELS MANAGEMENT, S.L.
          sus
          empleados, colaboradores o terceros.</li>
        <li>Incorpore datos de terceros sin su autorización.</li>
        <li>Sea reiterativo.</li>
        <li>Contenga cualquier tipo de recomendación en beneficio del usuario o terceros, sean personas físicas o
          jurídicas.</li>
        <li>No esté relacionado con el objeto del sitio, página o perfil de la red social.</li>
      </ol>
       </li>
  
    <li>BERTA MODELS MANAGEMENT, S.L., no se hace responsable de las opiniones vertidas en estos sitios, páginas o
      perfiles
      y no asume garantía alguna sobre la veracidad, exactitud o actualización de las informaciones en él contenidos.</li>
  
    <li>En ningún caso, BERTA MODELS MANAGEMENT, S.L. empleados y personal autorizado serán responsables de cualquier
      tipo
      de perjuicio, pérdidas, reclamaciones o gastos de ningún tipo, tanto si proceden como si no del uso de la red
      social,
      de
      la información adquirida o accedida por o a través de ésta, de virus informáticos, de fallos operativos o de
      interrupciones en el servicio o transmisión o fallos en la línea; el uso de esta red social, tanto por conexión
      directa
      como por vínculo u otro medio, constituye un aviso a cualquier usuario de que estas posibilidades pueden ocurrir.
    </li>
  </ol>
  
  <p>BERTA MODELS MANAGEMENT, S.L. se reserva el derecho a modificar, suspender, cancelar o restringir el contenido de
    este
    perfil, los vínculos o la información obtenida a través de ella, sin necesidad de previo aviso. En caso de que te
    surjan
    dudas relacionadas con navegación, contenido y acceso, por favor, contacta con <a
      href="mailto:berta@bertamodels.com">berta@bertamodels.com</a></p>`;

}
