import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { LayoutService } from '@app/layout/layout.service';
import { WINDOW } from '@app/window.service';
import {
  ModelsInTownQuery,
  ModelsInTownQuery_modelsInTown,
} from '@app/apollo/types/ModelsInTownQuery';

@Component({
  selector: 'app-models-in-town-content',
  templateUrl: 'models-in-town.component.html',
  styleUrls: ['models-in-town.component.sass'],
})
export class ModelsInTownComponent implements OnInit {
  modelsInTown: ModelsInTownQuery_modelsInTown[];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly titleService: Title,
    private readonly metaService: Meta,
    private readonly layoutService: LayoutService,
    @Inject(WINDOW) private readonly window: Window,
  ) {}

  ngOnInit(): void {

    const { query } = this.route.snapshot.data;
    const { content, modelsInTown } = query as ModelsInTownQuery;
    const { title, meta } = content;

    this.modelsInTown = modelsInTown;

    // SEO

    this.titleService.setTitle(`Berta Models - ${title}`);

    meta.forEach(m => {
      const { key, value } = m;
      this.metaService.updateTag({ name: key, content: value });
    });

    this.window.scrollTo(0, 0);
  }

}
