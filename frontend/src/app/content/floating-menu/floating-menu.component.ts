import {Component} from '@angular/core';

@Component({
  selector: 'floating-menu',
  template: `
    <div class="web-container"><ng-content></ng-content></div>
  `,
  styleUrls: ['./floating-menu.component.sass'],
})
export class FloatingMenuComponent {}
