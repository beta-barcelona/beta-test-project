import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AppConfigService } from '@app/app-config.service';
import { CONFIG } from './globals';
import { ResponsiveService } from '@app/responsive.service';
import { Apollo } from 'apollo-angular';
import { loadOldDataMutation } from './app.graphql';
import { LoadOldDataMutation } from './apollo/types/LoadOldDataMutation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  isCookieVisible: boolean = false;
  isMobile: boolean;
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.setIsMobile();
  }

  constructor(
    private appConfig: AppConfigService,
    private router: Router,
    private responsiveService: ResponsiveService,
    private readonly apollo: Apollo
  ) {}

  ngOnInit(): void {
    // register the dynamic routes
    this.appConfig.routes.forEach(r => {
      this.router.config.unshift(r);
    });

    this.setIsMobile();

  }

  setIsMobile() {
    this.isMobile = window.innerWidth <= CONFIG.mobileBreakpoint ? true : false;
    // update service
    this.responsiveService.onMobileDisplay(this.isMobile);
    this.responsiveService.displayChanged.emit(this.isMobile);
  }
}
