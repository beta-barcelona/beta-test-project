import { EventEmitter } from '@angular/core';

export class ResponsiveService {
  isMobile: boolean;
  displayChanged = new EventEmitter<boolean>();

  onMobileDisplay = (isMobile: boolean) => (this.isMobile = isMobile);
  getIsMobile = () => this.isMobile;
}
