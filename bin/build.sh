#!/bin/bash -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$(cd ${SCRIPT_DIR}/..; pwd)

FRONTEND_ENV=${1:-staging}

VERSION=${2:-$(git branch | grep \* | cut -d ' ' -f2)}

echo "Version: ${VERSION}"


cd ${ROOT_DIR}

FRONTEND_COMPONENTS=('admin' 'frontend')

for COMPONENT in "${FRONTEND_COMPONENTS[@]}"; do

    echo "Building ${COMPONENT} with frontend env = ${FRONTEND_ENV}..."

    docker build -t repo.treescale.com/betabcn/bertamodels/${COMPONENT}:${VERSION} \
        --build-arg ENVIRONMENT=${FRONTEND_ENV} \
        -f deploy/${COMPONENT}/Dockerfile .

    echo "Pushing ${COMPONENT}..."

    docker push repo.treescale.com/betabcn/bertamodels/${COMPONENT}:${VERSION}

done

BACKEND_COMPONENTS=('api' 'migrator')

for COMPONENT in "${BACKEND_COMPONENTS[@]}"; do

    echo "Building ${COMPONENT}..."
    docker build -t repo.treescale.com/betabcn/bertamodels/${COMPONENT}:${VERSION} -f deploy/${COMPONENT}/Dockerfile .

    echo "Pushing ${COMPONENT}..."
    docker push repo.treescale.com/betabcn/bertamodels/${COMPONENT}:${VERSION}

done



